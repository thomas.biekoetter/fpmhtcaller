#include <string>
#include "Higgs/Bounds.hpp"
#include "Higgs/Signals.hpp"

class HiggsToolsCaller {

  public:

      HiggsToolsCaller(
        int numh0,
        int numhp);
      ~HiggsToolsCaller();

      double checkpoint(
          double mh1,
          double mh2,
          double mhp) const;

      Higgs::bounds::Bounds bounds;
      Higgs::signals::Signals signals;

  private:
      int numh0;
      int numhp;

};
