#ifdef __cplusplus
extern "C" {
    class HiggsToolsCaller;
    typedef HiggsToolsCaller HIGGSTOOLSCALLER;
#else
    typedef struct HIGGSTOOLSCALLER HIGGSTOOLSCALLER;
#endif


// Constructor
HIGGSTOOLSCALLER* create_higgstoolscaller(
    int numh0, int numhp);


// Destructor
void delete_higgstoolscaller(HIGGSTOOLSCALLER* HiggsToolsCaller);


double higgstoolscaller_checkpoint(
    const HIGGSTOOLSCALLER* HiggsToolsCaller,
    double mh1,
    double mh2,
    double mh3);


#ifdef __cplusplus
}
#endif
