#include "Higgs/Bounds.hpp"
#include "Higgs/Predictions.hpp"
#include "Higgs/Signals.hpp"
#include "HiggsToolsCaller.hpp"
#include <iostream>


using namespace std;


namespace HP = Higgs::predictions;


#define STRINGIFY(x) #x
#define STRINGIFYMACRO(y) STRINGIFY(y)


HiggsToolsCaller::HiggsToolsCaller(int _numh0, int _numhp):
    bounds(Higgs::Bounds(STRINGIFYMACRO(hbdataset))),
    signals(Higgs::Signals(STRINGIFYMACRO(hsdataset))),
    numh0(_numh0),
    numhp(_numhp){
#ifdef hbdataset
        cout << "The HiggsBounds data path is:" << endl;
        cout << "    " << STRINGIFYMACRO(hbdataset) << endl;
#endif
#ifdef hbdataset
        cout << "The HiggsSignals data path is:" << endl;
        cout << "    " << STRINGIFYMACRO(hsdataset) << endl;
#endif
}


HiggsToolsCaller::~HiggsToolsCaller(){
    // Put stuff here if needed (free pointers or so)
}


double HiggsToolsCaller::checkpoint(
    double mh1,
    double mh2,
    double mhp) const{

    auto pred = Higgs::Predictions{};
    auto &h1 = pred.addParticle(
        HP::BsmParticle{"h1", HP::ECharge::neutral, HP::CP::even});
    auto &h2 = pred.addParticle(
        HP::BsmParticle{"h2", HP::ECharge::neutral, HP::CP::even});
    h1.setMass(mh1);
    h2.setMass(mh2);
    effectiveCouplingInput(
        h1,
        HP::smLikeEffCouplings,
        HP::ReferenceModel::SMHiggsEW);
    effectiveCouplingInput(
        h2,
        HP::smLikeEffCouplings,
        HP::ReferenceModel::SMHiggsEW);

    const auto resultHB = bounds(pred);
    std::cout << resultHB << std::endl;
    std::cout << "All applied limits: obsRatio (expRatio)\n";
    for (const auto &al : resultHB.appliedLimits) {
        std::cout << al.limit()->id() << " " << al.limit()->processDesc()
                  << ": " << al.obsRatio() << " (" << al.expRatio() << ")"
                  << std::endl;
    }

    auto resultHS = signals(pred);
    std::cout << "\n HiggsSignals chisq: " << resultHS << " from "
              << signals.observableCount() << " observables" << std::endl;

    return resultHS;

}
