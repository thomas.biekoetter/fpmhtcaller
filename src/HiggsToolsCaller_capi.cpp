#include "HiggsToolsCaller.h"
#include "HiggsToolsCaller.hpp"
#include <iostream>


using namespace std;


HIGGSTOOLSCALLER* create_higgstoolscaller(
    int numh0, int numhp){

    return new HiggsToolsCaller(numh0, numhp);

}


void delete_higgstoolscaller(
    HIGGSTOOLSCALLER* HiggsToolsCaller){

    delete HiggsToolsCaller;

}


double higgstoolscaller_checkpoint(
    const HIGGSTOOLSCALLER* HiggsToolsCaller,
    double mh1,
    double mh2,
    double mh3){

    return HiggsToolsCaller->checkpoint(mh1, mh2, mh3);

}
