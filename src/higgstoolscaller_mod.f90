module libhiggstoolscaller

  use iso_c_binding

  private
  public :: higgstoolscaller

  interface

    function create_higgstoolscaller_c(  &
      numh0, numhp) bind(C, name="create_higgstoolscaller")

      use iso_c_binding

      implicit none

      type(c_ptr) :: create_higgstoolscaller_c
      integer(c_int), value :: numh0
      integer(c_int), value :: numhp

    end function

    subroutine delete_higgstoolscaller_c(  &
      higgstoolscaller) bind(C, name="delete_higgstoolscaller")

      use iso_c_binding

      implicit none

      type(c_ptr), value :: higgstoolscaller

    end subroutine

    function higgstoolscaller_checkpoint_c(  &
      higgstoolscaller, mh1, mh2, mh3)  &
      bind(C, name="higgstoolscaller_checkpoint")

      use iso_c_binding

      implicit none

      real(kind=c_double) :: higgstoolscaller_checkpoint_c
      ! Use intent(in) for const variables in C
      ! Use value because Fortran by default passes by reference
      type(c_ptr), intent(in), value :: higgstoolscaller
      real(kind=c_double), value :: mh1
      real(kind=c_double), value :: mh2
      real(kind=c_double), value :: mh3

    end function

  end interface

  ! Use Fortan derived type to represent the C++ class
  type higgstoolscaller
      private
      type(c_ptr) :: ptr ! pointer to the HiggsToolsCaller class
  contains
      final :: delete_higgstoolscaller ! Destructor needed for gfortran
      procedure :: delete => delete_higgstoolscaller_polymorph
      procedure :: checkpoint => higgstoolscaller_checkpoint
  end type

  ! Constructor for higgstoolscaller
  interface higgstoolscaller
      procedure create_higgstoolscaller
  end interface

! Wrapper for the C functions
contains

  function create_higgstoolscaller(numh0, numhp)

    implicit none

    type(higgstoolscaller) :: create_higgstoolscaller
    integer, intent(in) :: numh0, numhp

    create_higgstoolscaller%ptr = create_higgstoolscaller_c(numh0, numhp)

  end function

  subroutine delete_higgstoolscaller(this)

    implicit none

    type(higgstoolscaller) :: this

    call delete_higgstoolscaller_c(this%ptr)

  end subroutine

  subroutine delete_higgstoolscaller_polymorph(this)

    implicit none

    class(higgstoolscaller) :: this

    call delete_higgstoolscaller_c(this%ptr)

  end subroutine

  real(kind=c_double) function higgstoolscaller_checkpoint(  &
    this, mh1, mh2, mh3)

    implicit none

    class(higgstoolscaller), intent(in) :: this
    real(kind=c_double), intent(in) :: mh1
    real(kind=c_double), intent(in) :: mh2
    real(kind=c_double), intent(in) :: mh3

    higgstoolscaller_checkpoint = higgstoolscaller_checkpoint_c(  &
      this%ptr,  &
      mh1,  &
      mh2,  &
      mh3)

  end function

end module libhiggstoolscaller
