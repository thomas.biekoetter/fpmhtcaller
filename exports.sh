HBDATAPATH="/itp/swift/biekoetter/Data/hbdataset"
HSDATAPATH="/itp/swift/biekoetter/Data/hsdataset"

HTDIR="/itp/swift/biekoetter/Tools/HiggsTools/higgstools"

FPM_CXXFLAGS="-Wall -Wextra -L$HTDIR/build/src -lHiggsTools"
FPM_CXXFLAGS="$FPM_CXXFLAGS -I$HTDIR/include -I$HTDIR/build/include"
FPM_CXXFLAGS="$FPM_CXXFLAGS -Dhbdataset=$HBDATAPATH -Dhsdataset=$HSDATAPATH"
export FPM_CXXFLAGS

FPM_FFLAGS="-Wall -Wextra -lstdc++ -L$HTDIR/build/src -lHiggsTools"
FPM_FFLAGS="$FPM_FFLAGS -I$HTDIR/include -I$HTDIR/build/include"
export FPM_FFLAGS

LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$HTDIR/build/src"
export LD_LIBRARY_PATH
