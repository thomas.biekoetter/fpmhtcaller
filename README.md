# htcaller

A template implementation of a Fortran derived type that
calls a C++ routine using the [HiggsTools] library.
Arguments are interchanged between Fortran and C++
via a C interface, using the iso_c_binding module
on the Fortran side.

In this example, we create a C++ class called
`HiggsToolsCaller`, which is exposed to the Fortran
derived type `higgstoolscaller`. This type has
three routines:

1. An initializer, which in the
current example initializes the HiggsTools
`bounds()` and `signals()` objects. The path to the
datafiles are set by environment variables before
compilation.
2. A routine that checks an example
parameter point. This routine creates an HiggsTools
`pred()` object and can then call `bounds(pred)` and
`signals(pred)`.
3. A desctructor, which is required
to be called by the user if gfortran is used as
a compiler. Whether the desctructor is actually
needed to avoid memory leaks depends on the
example at hand.

In our example, the actual implementation of the
HiggsTools features is contained in the function
`checkpoint()` defined in HiggsToolsCaller.cpp.
It takes the masses of two neutral and one charged
Higgs boson as input (the latter is not used), and
then performs a check against a scenario with two
Higgs bosons with SM-like couplings.

# Compilation

This example is structured as an fpm project. The source
and include files are contained in the directory `src`.
The interface can be used as a library in other fpm projects,
or one can directly implement a main program using the library
in the `app` folder.

We assume that HiggsTools is installed on the machine,
and that the HiggsBounds and HiggsSignals data
repositories have been downloaded. The first step
before compiling is to update the following enviornment variables
in the file export.sh:

1. `HBDATAPATH`: Path to the HiggsBounds data repository.
2. `HSDATAPATH`: Path to the HiggsSignals data repository.
3. `HTDIR`: Pah to the HiggsTools main directory which contains
the build folder.

After setting these variables, the `export.sh` file has to be sourced:
```
source export.sh
```
This sets the compiler flags pointing to the include files and the
shared library file of HiggsTools.

Now the package can be built with:

```
fpm build
```

The example program `test.F90` can be run with:

```
fpm run
```

# User instructions

Using this interface for a particular model requires to
implement the predictions for the model in the `checkpoint()`
C++ function. Within this function, the whole functionality
of HiggsTools can be used as in any C++ program.
Model-specific parameters like masses, couplings, cross sections
or branching ratios can be transferred from Fortran to C++
as arguments of the checkpoint routine. This requires to add
the arguments in several places:

1. In the C++ source and include files `src/HiggsToolsCaller.cpp` and
`src/include/HiggsToolsCaller.hpp`.
2. In the C API source and include files `src/HiggsToolsCaller_capi.cpp`
and `src/include/HiggsToolsCaller.h`.
3. Twice in the Fortran module (in the definition of the derived
type and the interface block) in the source file
`src/higgstoolscaller_mod.f90`.

Note that the transfer of arguments relies on the `iso_c_bindings`,
such that it is required to define the Fortran variables of type
`c_int`, `c_double`, `c_ptr`, etc.

[HiggsTools]: https://gitlab.com/higgsbounds/higgstools
